#define UNICODE // Force Unicode character sets
#define _UNICODE // Force Unicode character sets. _ prefix in the event UNICODE directive not supported
#include <Windows.h>

HINSTANCE m_AppInstance; // Application instance handle
HWND m_MainWindow; // Main window handle

LPWSTR m_ClassName = L"MyWindowClassName"; // Main window class name (Really, unused)
LPWSTR m_WindowTitle = L"My Window"; // Title displayed at top of window

// Window Procedure prototype.
// Implementation can be found below WinMain method
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain method extends WINAPI class.
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wcx; // Window Class Struct
	MSG msg; // Messaging struct
	
	// Window Class Struct definitions
	wcx.cbSize = sizeof(WNDCLASSEX); // Allocate memory size
	wcx.style = 0; // Default window style
	wcx.cbClsExtra = 0; // Hardly used
	wcx.cbWndExtra = 0; // Hardly used
	wcx.hInstance = m_AppInstance; // Set struct instance to app instance handle
	wcx.hIcon = LoadIcon(NULL, IDI_APPLICATION); // Set icon for task bar
	wcx.hIconSm = LoadIcon(NULL, IDI_APPLICATION); // Set icon for window title bar
	wcx.hCursor = LoadCursor(NULL, IDC_ARROW); // Load cursor to display. Regular mouse loaded
	wcx.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1); // Set window background to a grayish color
	wcx.lpszMenuName = NULL; // No window menu. Set to NULL
	wcx.lpszClassName = m_ClassName; // Window class
	wcx.lpfnWndProc = WndProc; // set procedure the window uses.
	
	// Check if window registration succeeded.
	// Alert the user if it fails
	if(!RegisterClassEx(&wcx))
	{
		MessageBox(NULL, L"Window Registration Failed!", L"Error", MB_OK | MB_ICONERROR);
		return 0;
	}
	
	// Create window
	m_MainWindow = CreateWindowEx(
		0, // WIndow style, set to default
		m_ClassName, // window class name
		m_WindowTitle, // window title
		WS_OVERLAPPEDWINDOW, // force overlapped window. Allows windows to move over others
		CW_USEDEFAULT, CW_USEDEFAULT, // Place window x and y coordinates where ever Windows wants to
		500, 400, // Set window width and height
		NULL, // This is not a child window, so no parent. Set to NULL
		NULL, // No window menu, set to NULL
		m_AppInstance, // Window is part of this application instance
		NULL // Never used...ever. Why do they have this??????? WHYYYY???
	);
	
	// Check if window was created.
	// Alert user if window creation fails
	if(m_MainWindow == NULL)
	{
		MessageBox(NULL, "Window Creation Failed!", :"Error", MB_OK | MB_ICONERROR);
		return 0;
	}
	
	// Show the window, and update it
	ShowWindow(m_MainWindow, nCmdShow);
	UpdateWindow(m_MainWindow);
	
	// Main event loop. Processes system messages
	// Sent to/from Windows to process events.
	while(GetMessage(&msg, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	// When while loop breaks, return exit
	// Code to kill the application
	return msg.wParam;
}

// Window Procedure implementation.
// WndProc handles all messages sent to the application
// from either the user or Windows
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Switch/Case block using msg as param
	switch(msg)
	{
	case WM_LBUTTONDOWN: // Left mouse button click in window
		MessageBox(m_MainWindow, L"You clicked inside the window!", L"Message", MB_OK | MB_ICONINFORMATION);
		break;
	case WM_CLOSE: // Close message sent from clicking red X button in window title bar
		DestroyWindow(m_MainWindow);
		break;
	case WM_DESTROY: // Message to handle destroying the window on close
		PostQuitMessage(0);
		break;
	// WndProc is a sublet of the default WndProc. Be sure to return this
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	
	// Always return true unless message handled.
	// Makes sure window doesn't spontaneously combust.
	return true;
}